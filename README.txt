NAME
	linear_regression.py - linear regression model for classification

SYNOPSIS
	python3 [linear_regression.py | perceptron.py | svm-dota.py] [--train train_file --model model_file | --test test_file --model model_file]
	
DESCRIPTION
	Learns or applies a model for predicting the outcome of a game of DOTA 2 using the specified procedure (linear regression, perceptron, SVM).
	
	Both of train_file and test_file should be comma seperated value (.csv) files containing information for a game of DOTA 2.  When training, model_file should be the name of the file you want to output the model to.  When testing, model_file should be the name of the file you would like to read the model from.
	
    Notes:
        - For the svm-dota.py script, it relies on files in the libsvm-3.22/ directory (from the LibSVM package) that are included in the zip file with all of our code. Our script assumes this directory is contained in the same directory that the script is executed. 	
