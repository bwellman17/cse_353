import numpy as np
import numpy.linalg as la
import argparse


LABEL_COL = 0
FEAT_COL = 4


parser = argparse.ArgumentParser(description="Train or test perceptron")

group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--train', type=str, help="Path to the training data",
                   metavar='TRAINING_DATA')

group.add_argument('--test', type=str, help="Path to the testing data",
                   metavar='TESTING_DATA')

parser.add_argument('--model', type=str, required=True, metavar="MODEL",
                    help="Path to the model file, either to save or load")


def sign(x):
    return (1 if x > 0 else -1)


def clean_data(unclean):
    # UNCLEAN MUST BE A MATRIX
    # Get the labels
    labels = unclean[:, [0]]
    # Get the features + 1 additional column for bias
    data = unclean[:, 3:]
    # Set the 0th column to 1 for the bias
    data[:, 0] = 1
    return data, labels


class Perceptron(np.ndarray):
    def learn_from(self, features, ideal_label):
        self += self.eta * (ideal_label - self.label(features)) * features

    def label(self, features):
        return sign(self @ features)


def train_perceptron(data, labels):
    # Initialize a the weight vector to be all zeros
    perceptron = (np.zeros(data.shape[1])).view(Perceptron)
    perceptron.eta = 1
    # Zip the data and the labels so we can iterate over them together
    for datum, label in zip(data, labels):
        # Learn from the datum and label pair
        perceptron.learn_from(datum, label)
    return perceptron


def test_perceptron(data, labels, model):
    correct = 0
    total = 0
    for datum, label in zip(data, labels):
        prediction = model.label(datum)
        total += 1
        if prediction == label:
            correct += 1
    return correct/total


if __name__ == "__main__":
    args = parser.parse_args()
    if args.train:
        print('Loading data from', args.train)
        data, labels = clean_data(np.genfromtxt(args.train, delimiter=','))
        print('Training perceptron', args.model)
        p = train_perceptron(data=data, labels=labels)
        np.savetxt(args.model, p, delimiter=',')
        print('Done training')
    if args.test:
        print('Loading model', args.model)
        p = np.genfromtxt(args.model, delimiter=',').view(Perceptron)
        print('Loading test data', args.test)
        data, labels = clean_data(np.genfromtxt(args.test, delimiter=','))
        print('Testing perceptron', args.model)
        acc = test_perceptron(data=data, labels=labels, model=p)
        print('Accuracy:\t', acc)
