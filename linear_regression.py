import numpy as np
import numpy.linalg as la
import argparse


parser = argparse.ArgumentParser(description="Train or test linear regression")

group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--train', type=str, help="Path to the training data",
                   metavar='TRAINING_DATA')

group.add_argument('--test', type=str, help="Path to the testing data",
                   metavar='TESTING_DATA')

parser.add_argument('--model', type=str, required=True, metavar="MODEL",
                    help="Path to the model file, either to save or load")


def sign(x):
    return (1 if x > 0 else -1)


def clean_data(unclean):
    # UNCLEAN MUST BE A MATRIX
    # Get the labels
    labels = unclean[:, [0]]
    # Get the features + 1 additional column for bias
    data = unclean[:, 3:]
    # Set the 0th column to 1 for the bias
    data[:, 0] = 1
    return data, labels


class LinearRegression(np.ndarray):
    def label(self, features):
        # We're using linear regression for classification so we use sign
        return sign(self @ features)


def train_linear_regression(data, labels):
    d = np.matrix(data)
    t = d.transpose()
    l = np.matrix(labels)
    # calculate the the weights
    # we use the pseudo inverse because the matrix may be singular
    w_temp = la.pinv(t * d) * t * l
    # Convert it to a one dimensional array
    w = np.array(w_temp.flatten())[0]
    return w.view(LinearRegression)


def test_linear_regression(data, labels, model):
    correct = 0
    total = 0
    for datum, label in zip(data, labels):
        prediction = model.label(datum)
        total += 1
        if label == prediction:
            correct += 1
    return correct/total


if __name__ == "__main__":
    args = parser.parse_args()
    if args.train:
        print('Loading data from', args.train)
        data, labels = clean_data(np.genfromtxt(args.train, delimiter=','))
        print('Training linear regression', args.model)
        p = train_linear_regression(data=data, labels=labels)
        np.savetxt(args.model, p, delimiter=',')
        print('Done training')
    if args.test:
        print('Loading model', args.model)
        p = np.genfromtxt(args.model, delimiter=',').view(LinearRegression)
        print('Loading test data', args.test)
        data, labels = clean_data(np.genfromtxt(args.test, delimiter=','))
        print('Testing linear regression', args.model)
        acc = test_linear_regression(data=data, labels=labels, model=p)
        print('Accuracy:\t', acc)
