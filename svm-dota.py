import sys
import numpy as np
import math
import argparse


parser = argparse.ArgumentParser(description="Train or test linear regression")

group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--train', type=str, help="Path to the training data",
                   metavar='TRAINING_DATA')

group.add_argument('--test', type=str, help="Path to the testing data",
                   metavar='TESTING_DATA')

parser.add_argument('--model', type=str, required=True, metavar="MODEL",
                    help="Path to the model file, either to save or load")

parser.add_argument('--libsvm', type=str, required=True, metavar='LIBSVMPATH',
                    help="Path to the python file for libsvm")

args = parser.parse_args()


#sys.path.insert(0, "/home/bwellman/libsvm-3.22/python")
sys.path.insert(0, args.libsvm)
import svm as svmc
from svmutil import *


def train_svm_validate(data, v):
    chunk_size = len(data)//v
    cg = []
    c = 2**-1.3
    g_start = 2**-5.25
    best_tuple = None
    bt_acc = 0

    for i in range(5):
        c = c*(2**.1)
        g = g_start
        for j in range(5):
            g = g*(2**.25)
            cg.append((c, g))

    for i in range(25):
        print("Trying "+str(cg[i]))
        total = 0
        for j in range(v):
            chunk_index = chunk_size*j
            train_data = np.concatenate((data[:chunk_index], data[chunk_index+chunk_size:]))
            test_data = data[chunk_index:chunk_index+chunk_size]
            labels = []
            features = []
            for datum in train_data:
                labels.append(datum[0])
                arr = datum[4:]
                features.append(arr.tolist())
            # parameters we are validating, cost and gamma
            cgtuple = cg[i]
            prob = svmc.svm_problem(labels, features)
            pstring = '-t 2 -h 0 -c '+str(cgtuple[0])+' -g '+str(cgtuple[1])
            param = svmc.svm_parameter(pstring)
            m = svm_train(prob, param)
            y = []
            x = []
            for datum in test_data:
                y.append(datum[0])
                arr = datum[4:]
                x.append(arr.tolist())
            p_label, p_acc, p_val = svm_predict(y, x, m)
            total += p_acc[0] * chunk_size

        total = total/len(data)
        print("Classifier "+ str(cg[i]) + " has a "+ str(total)+ " perecent accuracy.")
        if total > bt_acc:
            bt_acc = total
            best_tuple = cg[i]

    return best_tuple


train = args.train is not None
test = args.test is not None
fpath = args.train if train else args.test

with open(fpath, "r") as f:
    if train:
        train_data = np.genfromtxt(f, delimiter=",")
        ''' USED FOR VALIDATION
        best_tuple = train_svm_validate(my_data, 5)
        print("C = 2^" +str(math.log(best_tuple[0], 2)))
        print("g = 2^" + str(math.log(best_tuple[1], 2)))
        '''
        labels = []
        features = []
        for datum in train_data:
            labels.append(datum[0])
            arr = datum[4:]
            features.append(arr.tolist())
        prob = svmc.svm_problem(labels, features)
        cgtuple = (2**(-.8), 2**(-4.5))  # These numbers were attained by 5 fold validation
        pstring = '-t 2 -h 0 -c ' + str(cgtuple[0]) + ' -g ' + str(cgtuple[1])
        param = svmc.svm_parameter(pstring)
        m = svm_train(prob, param)
        svm_save_model(args.model, m)


    elif test:
        test_data = np.genfromtxt(f, delimiter=",")
        m = svm_load_model(args.model)
        # testing
        y = []
        x = []
        for datum in test_data:
            y.append(datum[0])
            arr = datum[4:]
            x.append(arr.tolist())
        p_label, p_acc, p_val = svm_predict(y, x, m)
        # print(p_acc[0])
